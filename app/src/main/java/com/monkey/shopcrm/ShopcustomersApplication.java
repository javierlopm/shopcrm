package com.monkey.shopcrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopcustomersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopcustomersApplication.class, args);
	}

}

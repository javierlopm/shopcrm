package com.monkey.shopcrm.controllers.usr;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {
    @GetMapping("/hi")
    public String helloWorld() {
        return "Hello Agile Monkeys! :)";
    }
}
